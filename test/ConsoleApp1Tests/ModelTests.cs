using System;
using NUnit.Framework;
using Models;

namespace Tests
{
    [TestFixture]
    public class ModelTests
    {
        [Test]
        public void WhenDogTalkToOwner_ThenWoofIsReturned()
        {
            // Arrange
            var expected = "Woof!";

            // Act
            var actual = new Dog().TalkToOwner();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void WhenCatTalkToOwner_ThenMeowIsReturned()
        {
            // Arrange
            var expected = "Meow!";

            // Act
            var actual = new Cat().TalkToOwner();

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}