﻿using System;
using System.Collections.Generic;
using Models;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var pets = new List<IPet>
            {
                new Dog(), new Cat()
            };

            foreach (var pet in pets)
            {
                Console.WriteLine(pet.TalkToOwner());
            }
        }
    }
}
