using System;

namespace Models
{
    public class Cat : IPet
    {
        public string TalkToOwner() => "Meow!";
    }
}