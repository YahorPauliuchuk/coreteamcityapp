using System;

namespace Models
{
    public class Dog : IPet
    {
        public string TalkToOwner() => "Woof!";
    }
}