using System;

namespace Models
{
    public interface IPet
    {
        string TalkToOwner();
    }
}