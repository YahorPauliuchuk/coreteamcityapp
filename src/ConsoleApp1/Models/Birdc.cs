using System;

namespace Models
{
    public class Bird : IPet
    {
        public string TalkToOwner() => "Cheerick!";

        public bool IsFlying { get; } = false;
    }
}